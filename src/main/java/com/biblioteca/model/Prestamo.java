package com.biblioteca.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "prestamo")
public class Prestamo implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_prestamo")
	private Integer idPrestamo;

	@Column(name = "fecha_prestamo", nullable = false)
	private LocalDateTime fecPrestamo;

	@Column(name = "fecha_devolucion", nullable = false)
	private LocalDateTime fecDevolucion;

	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name="id_libro")
	private Libro libro;
	
	public Integer getIdPrestamo() {
		return idPrestamo;
	}
	
	

	public Libro getLibro() {
		return libro;
	}



	public void setLibro(Libro libro) {
		this.libro = libro;
	}



	public void setIdPrestamo(Integer idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public LocalDateTime getFecPrestamo() {
		return fecPrestamo;
	}

	public void setFecPrestamo(LocalDateTime fecPrestamo) {
		this.fecPrestamo = fecPrestamo;
	}

	public LocalDateTime getFecDevolucion() {
		return fecDevolucion;
	}

	public void setFecDevolucion(LocalDateTime fecDevolucion) {
		this.fecDevolucion = fecDevolucion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	

}
