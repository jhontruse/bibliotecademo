package com.biblioteca.util;

import java.util.logging.Logger;

public class Loggers {

	private final static Logger LOGGER = Logger.getLogger(Loggers.class.getName());

	public static void msjeExito(String msje, String valor) {

		LOGGER.info(msje + " - valor " + valor);

	}

	public static void msjeError(String msje, String valor) {

		LOGGER.warning(msje + " - valor " + valor);

	}

}
