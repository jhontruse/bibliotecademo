package com.biblioteca.service.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import com.biblioteca.dao.IUsuarioRolDAO;
import com.biblioteca.model.Rol;
import com.biblioteca.model.Usuario;
import com.biblioteca.model.UsuarioRol;
import com.biblioteca.service.IUsuarioRolService;

@Named
public class UsuarioRolServiceImpl implements IUsuarioRolService, Serializable{
	
	@EJB //@Inject
	private IUsuarioRolDAO dao;

	@Override
	public Integer create(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public Integer update(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public Integer delete(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public List<UsuarioRol> list() throws Exception {
		return null;
	}

	@Override
	public UsuarioRol listId(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public Integer createUsuarioRol(Usuario usuario, List<Rol> roles) {
		
		try {
			dao.createUsuarioRol(usuario, roles);
			return 1;
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}
		
		
	}

}
