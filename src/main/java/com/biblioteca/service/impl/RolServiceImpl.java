package com.biblioteca.service.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import com.biblioteca.dao.IRolDAO;
import com.biblioteca.model.Rol;
import com.biblioteca.service.IRolService;

@Named
public class RolServiceImpl implements IRolService, Serializable{
	
	@EJB //@Inject
	private IRolDAO dao;

	@Override
	public Integer create(Rol t) throws Exception {
		return dao.create(t);
	}

	@Override
	public Integer update(Rol t) throws Exception {
		return null;
	}

	@Override
	public Integer delete(Rol t) throws Exception {
		return null;
	}

	@Override
	public List<Rol> list() throws Exception {
		return dao.list();
	}

	@Override
	public Rol listId(Rol t) throws Exception {
		return dao.listId(t);
	}

}
