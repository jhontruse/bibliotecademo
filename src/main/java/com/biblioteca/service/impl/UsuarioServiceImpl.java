package com.biblioteca.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;

import com.biblioteca.dao.IUsuarioDAO;
import com.biblioteca.model.Usuario;
import com.biblioteca.service.IUsuarioService;
import com.biblioteca.util.MensajeManager;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable {

	@EJB // @Inject
	private IUsuarioDAO dao;
	
	@Inject
	private MensajeManager mensajeManager;

	@Override
	public Integer create(Usuario t) throws Exception {

		try {
			if (dao.create(t) ==1) {
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}

	}

	@Override
	public Integer update(Usuario t) throws Exception {

		try {
			if (dao.update(t) ==1) {
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}
	}

	@Override
	public Integer delete(Usuario t) throws Exception {

		try {
			if (dao.delete(t) ==1) {
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}
	}

	@Override
	public List<Usuario> list() throws Exception {

		return dao.list();
	}

	@Override
	public Usuario listId(Usuario t) throws Exception {

		return dao.listId(t);
	}

}
