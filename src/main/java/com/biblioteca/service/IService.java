package com.biblioteca.service;

import java.util.List;

public interface IService<T> {

	Integer create(T t) throws Exception;
	Integer update(T t) throws Exception;
	Integer delete(T t) throws Exception;
	List<T> list() throws Exception;
	T listId(T t) throws Exception; 
}
