package com.biblioteca.service;

import java.util.List;

import com.biblioteca.model.Rol;
import com.biblioteca.model.Usuario;
import com.biblioteca.model.UsuarioRol;

public interface IUsuarioRolService extends IService<UsuarioRol>{
	
	public Integer createUsuarioRol(Usuario usuario, List<Rol> roles);

}
