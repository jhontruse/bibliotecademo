package com.biblioteca.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.MoveEvent;
import org.primefaces.event.RowEditEvent;

import com.biblioteca.model.Usuario;
import com.biblioteca.service.IRolService;
import com.biblioteca.service.IUsuarioRolService;
import com.biblioteca.service.IUsuarioService;
import com.biblioteca.util.MensajeManager;

@Named
@ViewScoped
public class UpdateUserBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;

	@Inject
	private IUsuarioRolService usuarioRolService;

	@Inject
	private IRolService rolService;

	@Inject
	private MensajeManager mensajeManager;

	private List<Usuario> usuarios;

	private Usuario usuario;

	private String tipoDialog;
	
	private Usuario selectedUsuario;

	@PostConstruct
	public void init() {

		this.listar();
		this.tipoDialog = "Nuevo";
		this.usuario = new Usuario();
		

	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {
				this.usuario.setFoto(event.getFile().getContents());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void mostrarData(Usuario p) {
		this.usuario = p;
		this.tipoDialog = "Modificar";
	}
	
	public void mostrarDataDetalle(Usuario p) {
		this.usuario = p;
		this.tipoDialog = "Detalle";
	}


	public String operar(String accion) {
		try {

			if (accion.equalsIgnoreCase("M")) {
				
				if (!usuario.getContrasena().equalsIgnoreCase("")) {
					String clave = this.usuario.getContrasena();
					String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
					this.usuario.setContrasena(claveHash);
				}
				
				if (usuarioService.update(this.usuario) == 1) {
					mensajeManager.mostrarMensaje("AVISO", "Se actualizo con exito", "INFO");
				} else {
					this.mensajeManager.mostrarMensaje("AVISO", "Ocurrio un incidente", "WARN");
				}
			}
			this.listar();
		} catch (Exception e) {
			e.printStackTrace();
			this.mensajeManager.mostrarMensaje("AVISO", "Ocurrio un incidente", "FATAL");
		}
		return "index?faces-redirect=true";
	}
	
	
	public void delete(Usuario usu) {
		
		try {
			if (usuarioService.delete(usu) == 1) {
				this.listar();
				mensajeManager.mostrarMensaje("AVISO", "Se elimino con exito", "INFO");
				
			} else {
				mensajeManager.mostrarMensaje("AVISO", "Ocurrio un error", "FATAL");
				
			}
		} catch (Exception e) {
			mensajeManager.mostrarMensaje("AVISO", "Ocurrio un error", "FATAL");
		}
		
	}
	
	
	public Usuario getSelectedUsuario() {
		return selectedUsuario;
	}

	public void setSelectedUsuario(Usuario selectedUsuario) {
		this.selectedUsuario = selectedUsuario;
	}

	public void listar() {
		try {
			this.usuarios = this.usuarioService.list();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public String getTipoDialog() {
		return tipoDialog;
	}

	public void setTipoDialog(String tipoDialog) {
		this.tipoDialog = tipoDialog;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

}
