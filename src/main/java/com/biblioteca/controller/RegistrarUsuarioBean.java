package com.biblioteca.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;
import org.primefaces.event.FileUploadEvent;
import com.biblioteca.model.Rol;
import com.biblioteca.model.Usuario;
import com.biblioteca.service.IRolService;
import com.biblioteca.service.IUsuarioRolService;
import com.biblioteca.service.IUsuarioService;
import com.biblioteca.util.Loggers;
import com.biblioteca.util.MensajeManager;

@Named
@ViewScoped
public class RegistrarUsuarioBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;

	@Inject
	private IUsuarioRolService usuarioRolService;

	@Inject
	private IRolService rolService;

	@Inject
	private MensajeManager mensajeManager;

	private Usuario usuario;

	private List<Rol> roles;

	private String[] roles2;

	@PostConstruct
	public void init() {

		this.listar();
		this.usuario = new Usuario();
		usuario.setEstado("A");
	}

	public void listar() {
		try {
			this.roles = this.rolService.list();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Transactional
	public String regtistrar() {

		List<Rol> roles3 = new ArrayList<>();

		try {

			for (int i = 0; i < roles2.length; i++) {

				Rol rol4 = new Rol();

				rol4.setIdRol(Integer.parseInt(roles2[i]));

				roles3.add(i, rol4);

			}

			usuario.setIdUsuario(1);
			
			String clave = this.usuario.getContrasena();
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setContrasena(claveHash);

			if (usuarioService.create(usuario) == 1 && usuarioRolService.createUsuarioRol(usuario, roles3) == 1) {
				mensajeManager.mostrarMensaje("AVISO", "Se registro con exito", "INFO");
				return "index?faces-redirect=true";
			} else {
				mensajeManager.mostrarMensaje("AVISO", "Ocurrio un error", "FATAL");
				return "";
			}

		} catch (Exception e) {
			e.getMessage();
			mensajeManager.mostrarMensaje("AVISO", "Ocurrio un error", "FATAL");
			return "";
		}

		
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {
				this.usuario.setFoto(event.getFile().getContents());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String[] getRoles2() {
		return roles2;
	}

	public void setRoles2(String[] roles2) {
		this.roles2 = roles2;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	public MensajeManager getMensajeManager() {
		return mensajeManager;
	}

	public void setMensajeManager(MensajeManager mensajeManager) {
		this.mensajeManager = mensajeManager;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
