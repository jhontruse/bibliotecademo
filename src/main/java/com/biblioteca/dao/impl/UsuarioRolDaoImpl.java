package com.biblioteca.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.biblioteca.dao.IUsuarioRolDAO;
import com.biblioteca.model.Rol;
import com.biblioteca.model.Usuario;
import com.biblioteca.model.UsuarioRol;

@Stateless
public class UsuarioRolDaoImpl implements IUsuarioRolDAO, Serializable {

	@PersistenceContext(unitName = "dbBiblioteca")
	private EntityManager em;

	@Override
	public Integer create(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public Integer update(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public Integer delete(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public List<UsuarioRol> list() throws Exception {
		return null;
	}

	@Override
	public UsuarioRol listId(UsuarioRol t) throws Exception {
		return null;
	}

	@Override
	public Integer createUsuarioRol(Usuario usuario, List<Rol> roles) {

		try {

			for (int i = 0; i < roles.size(); i++) {

				Query q = em.createNativeQuery("  INSERT INTO  usuario_rol (id_rol, id_usuario) VALUES (?1, ?2) ");
				q.setParameter(1, roles.get(i).getIdRol());
				q.setParameter(2, usuario.getIdUsuario());
				q.executeUpdate();

			}
			return 1;
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}

	}

}
