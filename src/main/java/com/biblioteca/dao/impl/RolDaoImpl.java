package com.biblioteca.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.biblioteca.dao.IRolDAO;
import com.biblioteca.model.Rol;

@Stateless
public class RolDaoImpl implements IRolDAO, Serializable {

	@PersistenceContext(unitName = "dbBiblioteca")
	private EntityManager em;

	@Override
	public Integer create(Rol t) throws Exception {
		try {
			em.persist(t);
			return 1;
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}
		
	}

	@Override
	public Integer update(Rol t) throws Exception {

		return null;
	}

	@Override
	public Integer delete(Rol t) throws Exception {

		return null;
	}

	@Override
	public List<Rol> list() throws Exception {

		List<Rol> roles = new ArrayList<>();
		Query q = em.createNativeQuery(" SELECT * FROM db_libreria.rol ");
		List<Object[]> data = q.getResultList();
		for (int i = 0; i < data.size(); i++) {
			Rol rol = new Rol();
			rol.setIdRol(Integer.parseInt(String.valueOf(data.get(i)[0])));
			rol.setTipo(String.valueOf(data.get(i)[1]));
			roles.add(i,rol);
		}
		return roles;

		////////////////////////////////////////
		/*
		 * Query q = em.createQuery("SELECT r FROM Rol r"); List<Rol> lista =
		 * (List<Rol>) q.getResultList(); return lista;
		 */

	}

	@Override
	public Rol listId(Rol t) throws Exception {

		return null;
	}

}
