package com.biblioteca.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.biblioteca.dao.IUsuarioDAO;
import com.biblioteca.model.Rol;
import com.biblioteca.model.Usuario;
import com.biblioteca.util.Loggers;
import com.biblioteca.util.MensajeManager;

@Stateless
public class UsuarioDaoImpl implements IUsuarioDAO, Serializable {

	@PersistenceContext(unitName = "dbBiblioteca")
	private EntityManager em;

	@Inject
	private MensajeManager mensajeManager;

	@Override
	public Integer create(Usuario t) throws Exception {
		try {
			// em.persist(t);

			Query q = em.createNativeQuery(
					" INSERT INTO usuario (apellidos,contrasena,direccion,estado,nombres,sexo,usuario,foto)VALUES("
							+ "?1,?2,?3,?4,?5,?6,?7,?8)");
			q.setParameter(1, t.getApellidos());
			q.setParameter(2, t.getContrasena());
			q.setParameter(3, t.getDireccion());
			q.setParameter(4, t.getEstado());
			q.setParameter(5, t.getNombres());
			q.setParameter(6, t.getSexo());
			q.setParameter(7, t.getUsuario());
			q.setParameter(8, t.getFoto());
			q.executeUpdate();
			return 1;
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}

	}

	@Override
	public Integer update(Usuario t) throws Exception {
		try {
			// em.persist(t);
			if (t.getContrasena().equalsIgnoreCase("")) {
				Query q = em.createNativeQuery(
						" UPDATE usuario SET apellidos = ?1 , direccion = ?2 , estado = ?3 , nombres = ?4 , sexo = ?5 , usuario = ?6 , foto = ?7 WHERE id_usuario = ?8 ");
				q.setParameter(1, t.getApellidos());
				q.setParameter(2, t.getDireccion());
				q.setParameter(3, t.getEstado());
				q.setParameter(4, t.getNombres());
				q.setParameter(5, t.getSexo());
				q.setParameter(6, t.getUsuario());
				q.setParameter(7, t.getFoto());
				q.setParameter(8, t.getIdUsuario());
				
				q.executeUpdate();
				return 1;
			} else {
				Query q = em.createNativeQuery(
						" UPDATE usuario SET apellidos = ?1 , contrasena = ?2 , direccion = ?3 , estado = ?4 , nombres = ?5 , sexo = ?6 , usuario = ?7 , foto = ?8 WHERE id_usuario = ?9 ");
				q.setParameter(1, t.getApellidos());
				q.setParameter(2, t.getContrasena());
				q.setParameter(3, t.getDireccion());
				q.setParameter(4, t.getEstado());
				q.setParameter(5, t.getNombres());
				q.setParameter(6, t.getSexo());
				q.setParameter(7, t.getUsuario());
				q.setParameter(8, t.getFoto());
				q.setParameter(9, t.getIdUsuario());
				q.executeUpdate();
				return 1;
			}
			
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}
	}

	@Override
	public Integer delete(Usuario t) throws Exception {
		try {
			// em.persist(t);

			Query q = em.createNativeQuery(
					" UPDATE usuario SET estado = ?1 WHERE id_usuario = ?2 ");
			q.setParameter(1, "D");
			q.setParameter(2, t.getIdUsuario());
			q.executeUpdate();
			return 1;
		} catch (Exception e) {
			e.getMessage();
			return 0;
		}
	}

	@Override
	public List<Usuario> list() throws Exception {
		List<Usuario> usuarios = new ArrayList<>();
		try {

			Query q = em.createNativeQuery(" SELECT * FROM db_libreria.usuario ");
			List<Object[]> data = q.getResultList();
			for (int i = 0; i < data.size(); i++) {
				Usuario usuario = new Usuario();
				usuario.setIdUsuario(Integer.parseInt(String.valueOf(data.get(i)[0])));
				usuario.setApellidos(String.valueOf(data.get(i)[1]));
				// usuario.setContrasena(String.valueOf(data.get(i)[2]));
				usuario.setDireccion(String.valueOf(data.get(i)[3]));
				usuario.setEstado(String.valueOf(data.get(i)[4]));
				usuario.setFoto((byte[]) data.get(i)[5]);
				usuario.setNombres(String.valueOf(data.get(i)[6]));
				usuario.setSexo(String.valueOf(data.get(i)[7]));
				usuario.setUsuario(String.valueOf(data.get(i)[8]));

				usuarios.add(i, usuario);
			}

			return usuarios;

		} catch (Exception e) {
			e.getMessage();
			return usuarios;
		}

	}

	@Override
	public Usuario listId(Usuario t) throws Exception {
		Usuario usuario = new Usuario();
		try {

			Query q = em.createNativeQuery(" SELECT * FROM db_libreria.usuario where id_usuario = ?1");
			q.setParameter(1, t.getIdUsuario());

			List<Object[]> data = q.getResultList();

			for (int i = 0; i < data.size(); i++) {

				usuario.setIdUsuario(Integer.parseInt(String.valueOf(data.get(i)[0])));
				usuario.setApellidos(String.valueOf(data.get(i)[1]));
				// usuario.setContrasena(String.valueOf(data.get(i)[2]));
				usuario.setDireccion(String.valueOf(data.get(i)[3]));
				usuario.setEstado(String.valueOf(data.get(i)[4]));
				usuario.setFoto((byte[]) data.get(i)[5]);
				usuario.setNombres(String.valueOf(data.get(i)[6]));
				usuario.setSexo(String.valueOf(data.get(i)[7]));
				usuario.setUsuario(String.valueOf(data.get(i)[8]));

				
			}

			return usuario;

		} catch (Exception e) {
			e.getMessage();
			return usuario;
		}
	}

}
