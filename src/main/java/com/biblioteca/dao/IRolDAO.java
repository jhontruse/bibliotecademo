package com.biblioteca.dao;

import javax.ejb.Local;

import com.biblioteca.model.Rol;
@Local
public interface IRolDAO extends ICRUD<Rol>{

}
