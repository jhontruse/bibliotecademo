package com.biblioteca.dao;

import java.util.List;

import javax.ejb.Local;

import com.biblioteca.model.Rol;
import com.biblioteca.model.Usuario;
import com.biblioteca.model.UsuarioRol;
@Local
public interface IUsuarioRolDAO extends ICRUD<UsuarioRol>{
	
	public Integer createUsuarioRol(Usuario usuario, List<Rol> roles);

}
